﻿

define u = Character("Uta")
define d = Character("Uta's father")
define v = Character("Veronica")

image bg father smithery = "father smith background.jpg"
image bg street view = "street view.jpg"
image bg forrest = "Fantasy-Forrest.png"

label start:
    "At last..."
    "The church ceremony is sure a boring one."
    "I can't imagine how boring it would be for the others, who are still waiting for their identification card."
    "It's a requirement for all starting at the age of 18."
    "It's also when children are consider adults..."
    "And me is no exception."
    
    $ strength = 10
    $ intelligence = 10
    $ agility = 10
    
    u "My name is Uta, most people know me as the daughter of the blacksmith."
    u "The place we lived is the nearest village to the demon territory."
    u "Also known as Hellbone, for its visible baren terran not far away from the village."
    u "As a result, Hellbone has become a hotspot for adventurers and demon spies alike."
    u "We constantly have visitors coming accross our little town here..."
    u "And my dad's place is just right at the corner."
    u "The blacksmith family they say, my ancestors all made tools and weapons."
    u "But I want to travel the world!"
    u "I don't like staying in one place for too long."
    u "My father obviously doesn't like my adventurer side of things..."
    u "It remind him of my mother, who's one tough woman that travel alone to many parts of the world."
    u "He always like to joke about if I train hard, I am able to beat the strongest man in this village."
    u "Since my mother is at least 10 times stronger than big man already."
    
    "Location Blacksmith"
    scene bg father smithery
    d "Welcome home darling, how's the ceremony going?"
    menu:
        "It's fine... you know, nothing too crazy.":
            $ choice1 = "1"
            jump start1
        "I hope it can be much quicker":
            $ choice1 = "2"
            jump start2

label start1:
d "Sounds good?"
jump Act1

label start2:
d "Well, they are always busy at this time of the year."
jump Act1

label Act1:
    d "As you already know, it's about time you learn the more advanced stuff for our smithing family business."
    u "Right."
    d "Don't worry, you're smart enough to learn everything quickly!"
    u "Really? I thought you always scold me for messing up things."
    d "Despite all of that, your skills improve faster than I thought."
    d "I think you have a great potential in becoming a lengedary smith!"
    u "A lengedary girl smith huh."
    d "You don't like it?"
    u "Nah, I just prefer outdoors."
    u "I would like to not stay in the same place for too long, it's boring."
    d "Well, you know what it is."
    "Uta spend her time in the blacksmith"
    u "Dad, I finished my work for today."
    d "Sure! please get back before the sun set."
    u "I wonder how would the Hero party looks like..."
    u "They should be arriving shortly here near the church."
    
    scene bg street view
    u "Wow! Not expecting a crowd here..."
    "The Heroes are here! Make way! Make way!"
    u "Urgh, can't see."
    "Thank you for coming! I am the Hero the king has summoned!"
    "I will be preparing for my assualt against the evil demonic force!"
    "The time where humanity are threatened by demons is over!"
    u "I can only see his hair..."
    u "So hard to see..."
    "Now if you may please, unless you have urgent matters please do not disturb our Heroes."
    u "And it's over... shoot."
    v "Hey Uta."
    u "Huh."
    v "You really are forgetful, aren't you?"
    u "(Of course I know her, she's the young priest that worked for the church.)"
    u "(She is almost a star in our small village, as she's not only beautiful but also highly skilled.)"
    v "Ah looks like you begin to recall..."
    u "Hi Veronica, it's been a while I met you."
    v "Yes! and you are as dirty as I remembered, hehe."
    u "Hey..."
    v "Well, I see you are having difficulties in wanting to get near to our brave heroes."
    v "It's most unfortunate that you can't go near them."
    v "As I am the one that can most certaintly prove useful to our heroes with my skills."
    u "Urgh, come on Veronica."
    v "Hehe~ I look forward to that day when we met again to serve our heroes..."
    "Veronica has left."
    u "She's right, soon enough the heroes will recruit people across the village to aid in their quest against demons."
    u "Maybe I can use that opportunity to leave the village..."
    u "As well as roaming around! Oh that could be nice! Hehe."
    u "But before that, I will need to prepare..."
    
    "Location Forrest"
    scene bg forrest
    u "Should be safe from the demons, I used to hunt some small animals before."
    u "This place is just what I recall, but still, I need to be careful."
    "Wild boar sighted"
    "Battle system insert here"
    u "That's easier than I expected, the sun isn't set yet, looks like I can go hunt some more~"
    















return
