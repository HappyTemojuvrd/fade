﻿# TODO: Translation updated at 2020-07-27 22:49

# game/script.rpy:48
translate english start_61a88897:

    # v "It's quiet in here."
    v ""

# game/script.rpy:49
translate english start_888bdfdb:

    # v "Let's make this different."
    v ""

# game/script.rpy:57
translate english start_7b7b7c1b:

    # v "Pleased to meet you, [y]."
    v ""

# game/script.rpy:58
translate english start_b83ad773:

    # y "Yes?"
    y ""

# game/script.rpy:59
translate english start_a7c9b0fa:

    # v "You are the chosen one is this timeless realm of space."
    v ""

# game/script.rpy:60
translate english start_1606c9a0:

    # v "This is the void, it is also me, now, using a collective consciousness to communicating with you."
    v ""

# game/script.rpy:61
translate english start_138eba0a:

    # y "I see."
    y ""

# game/script.rpy:63
translate english start_d7e8bed1:

    # y "It doesn't look so empty in here."
    y ""

# game/script.rpy:64
translate english start_b7f51ecc:

    # v "It's only empty when no one is looking."
    v ""

# game/script.rpy:65
translate english start_33b64a71:

    # v "Like particles without the constraint of time, everthing can be timeless and spaceless as long as there's no need for existence."
    v ""

# game/script.rpy:66
translate english start_025700f6:

    # y "Are you god?"
    y ""

# game/script.rpy:67
translate english start_6a70e9b0:

    # v "No, I just the world itself."
    v ""

# game/script.rpy:68
translate english start_00555574:

    # v "Now ready to serve you, my new master."
    v ""

# game/script.rpy:76
translate english start1_7ca33170:

    # v "Yes, [y]."
    v ""

# game/script.rpy:80
translate english start2_978b0385:

    # v "I'm all yours, [y]."
    v ""

# game/script.rpy:85
translate english Act1_35e779e1:

    # v "You should get stronger, meanwhile I will guard this place."
    v ""

# game/script.rpy:86
translate english Act1_5f234f9d:

    # y "You mean I should train?"
    y ""

# game/script.rpy:87
translate english Act1_e29ad0c9:

    # v "Accurately, yes."
    v ""

# game/script.rpy:89
translate english Act1_d944d93b:

    # y "What is this blue world?"
    y ""

# game/script.rpy:90
translate english Act1_dc93811b:

    # y "Wait, I can see everything on it."
    y ""

# game/script.rpy:91
translate english Act1_978b9908:

    # v "Your power should be surging soon, do you want to try?"
    v ""

# game/script.rpy:92
translate english Act1_bff00816:

    # y "For sure, I can already feel it."
    y ""

# game/script.rpy:93
translate english Act1_129ae752:

    # y "The many paths for ascension."
    y ""

# game/script.rpy:98
translate english Act1_764a31c4:

    # "Location unknown."
    ""

# game/script.rpy:99
translate english Act1_8daab8ab:

    # "And we are done, have a good holiday."
    ""

# game/script.rpy:100
translate english Act1_9b5f7554:

    # "YEAH!"
    ""

# game/script.rpy:101
translate english Act1_b567e36e:

    # r "Hi [y], what are you waiting for, let's go."
    r ""

# game/script.rpy:102
translate english Act1_0c34d007:

    # y "(Raven, my friend, she is a girl with potential.)"
    y ""

# game/script.rpy:103
translate english Act1_fff2d6da:

    # y "Yes."
    y ""

# game/script.rpy:106
translate english Act1_839fcc6e:

    # r "You look blur today, is something wrong?"
    r ""

# game/script.rpy:107
translate english Act1_9f4c1f7c:

    # y "It's fine, I just think of something lately."
    y ""

# game/script.rpy:108
translate english Act1_a40bc13b:

    # r "Well we are turning 17 after this holiday is over."
    r ""

# game/script.rpy:109
translate english Act1_47b876e4:

    # r "I really do miss our time."
    r ""

# game/script.rpy:110
translate english Act1_d01ccf1b:

    # y "You do, you won your third karate champion just last month."
    y ""

# game/script.rpy:111
translate english Act1_e61bdeb4:

    # y "You really want to become a heroine, don't you?"
    y ""

# game/script.rpy:112
translate english Act1_3a69d980:

    # r "Yeah, most probably, a little awkward for me is that I don't have any super natural powers."
    r ""

# game/script.rpy:113
translate english Act1_5268141c:

    # y "Well you are super strong."
    y ""

# game/script.rpy:114
translate english Act1_57cc8119:

    # r "But I want cool powers! Like eyes laser, flight and such!"
    r ""

# game/script.rpy:115
translate english Act1_953641df:

    # r "Anyways let's leave all those problems behind for now, right now, I just want to have fun with you."
    r ""

# game/script.rpy:116
translate english Act1_136529cb:

    # y "Sure."
    y ""

# game/script.rpy:119
translate english Act1_390c4737:

    # y "(Lionstar Project.)"
    y ""

# game/script.rpy:120
translate english Act1_e3ecc92d:

    # y "(Raven is one of the many supers in the acadmic city of Area 52)"
    y ""

# game/script.rpy:121
translate english Act1_f5f7d9ac:

    # y "(A disclosure area where supers from every place around the world, and some even from other worlds.)"
    y ""

# game/script.rpy:122
translate english Act1_c2d4b388:

    # y "(I myself is also consider a super, but I didn't exposure my real identity here to anyone yet.)"
    y ""

# game/script.rpy:123
translate english Act1_14dcd336:

    # y "(So far things are looking good, and time spent with Raven always feel short for me.)"
    y ""

# game/script.rpy:126
translate english Act1_86ab4857:

    # r "Ah, it's already night now."
    r ""

# game/script.rpy:127
translate english Act1_fff2d6da_1:

    # y "Yes."
    y ""

# game/script.rpy:128
translate english Act1_2d9d8754:

    # r "Are you heading back soon?"
    r ""

# game/script.rpy:129
translate english Act1_adb247a6:

    # y "No, I don't have any plans."
    y ""

# game/script.rpy:130
translate english Act1_f325dad5:

    # r "I see."
    r ""

# game/script.rpy:131
translate english Act1_31901247:

    # r "[y]."
    r ""

# game/script.rpy:132
translate english Act1_b83ad773:

    # y "Yes?"
    y ""

# game/script.rpy:133
translate english Act1_97a32a0e:

    # r "Do you like me?"
    r ""

# game/script.rpy:134
translate english Act1_71a86863:

    # y "..."
    y ""

# game/script.rpy:135
translate english Act1_1bf41611:

    # y "I do like you."
    y ""

# game/script.rpy:136
translate english Act1_37c8f20a:

    # r "Hmm, I don't know."
    r ""

# game/script.rpy:137
translate english Act1_e8d3b34b:

    # r "I like you [y]."
    r ""

# game/script.rpy:138
translate english Act1_aac97d03:

    # r "I don't want to leave you for sure."
    r ""

# game/script.rpy:139
translate english Act1_fbad2b67:

    # y "Are you worried about me?"
    y ""

# game/script.rpy:140
translate english Act1_b9b755e5:

    # r "..."
    r ""

# game/script.rpy:141
translate english Act1_27d946d1:

    # y "Well I will always be here."
    y ""

# game/script.rpy:142
translate english Act1_93660b81:

    # r "Are you sure you can still be with me, while I go to the heroine academic?"
    r ""

# game/script.rpy:143
translate english Act1_5c64ac92:

    # y "Why not?"
    y ""

# game/script.rpy:144
translate english Act1_9a8879c1:

    # r "You are not as strong as I know!"
    r ""

# game/script.rpy:145
translate english Act1_c187147a:

    # r "I mean you may not make it."
    r ""

# game/script.rpy:146
translate english Act1_ceb2a090:

    # y "It's only a test that I need to do, it should be fine."
    y ""

# game/script.rpy:147
translate english Act1_1ecb954e:

    # r "If you say so..."
    r ""

# game/script.rpy:148
translate english Act1_c801ded7:

    # y "(Raven still feels sad for some reason.)"
    y ""

# game/script.rpy:149
translate english Act1_d37cb641:

    # y "Fine, I make a promise."
    y ""

# game/script.rpy:150
translate english Act1_efe39de2:

    # y "I will be there with you."
    y ""

# game/script.rpy:151
translate english Act1_0ddbdce1:

    # r "Really?"
    r ""

# game/script.rpy:152
translate english Act1_230c2c16:

    # y "Really."
    y ""

# game/script.rpy:153
translate english Act1_51668e17:

    # r "I suppose I'm such a silly one to be worry about you."
    r ""

# game/script.rpy:154
translate english Act1_3ba68749:

    # "Raven has left."
    ""

# game/script.rpy:155
translate english Act1_af9350c4:

    # y "Me too Raven."
    y ""

# game/script.rpy:158
translate english Act1_335314a0:

    # y "(I should be preparing.)"
    y ""

# game/script.rpy:159
translate english Act1_0fe14e1e:

    # y "(What training should I do?)"
    y ""

# game/script.rpy:180
translate english Stat_1cc1d4da:

    # "Your power is [power]"
    ""

# game/script.rpy:188
translate english T1_2e1ef07d:

    # "You plan to lift 10 tons."
    ""

# game/script.rpy:189
translate english T1_e2ffc2bc:

    # "You feel like your body is chilling at best."
    ""

# game/script.rpy:190
translate english T1_bf3654ed:

    # "As you move the weight, in a shape of dumbbells made by your goddess creation power."
    ""

# game/script.rpy:191
translate english T1_f89b28d3:

    # "You lift it once, and you already feel your body in full emerged in power."
    ""

# game/script.rpy:195
translate english T1_25e45cca:

    # "Next is 100 tons."
    ""

# game/script.rpy:196
translate english T1_9114431e:

    # "It's the same shape of dumbbells, and the floor is clearing crying."
    ""

# game/script.rpy:197
translate english T1_22cd0fa5:

    # "You lift it carefully, almost trying to feel it's power."
    ""

# game/script.rpy:198
translate english T1_bd593b2e:

    # "So far, you feel unchallenged."
    ""

# game/script.rpy:202
translate english T1_3799509e:

    # "1000 tons sound like much, but you know you can do more."
    ""

# game/script.rpy:203
translate english T1_5c7160bc:

    # "This time it's in the shape of barbells."
    ""

# game/script.rpy:204
translate english T1_1c5f0a83:

    # "It feels as easy as before, only this time, the ground can't withstand and collapsed."
    ""

# game/script.rpy:205
translate english T1_077c9c68:

    # "You quickly fix it before things become out of hand."
    ""

# game/script.rpy:209
translate english T1_9e4db324:

    # "There's no heavy enough equipment for you to train."
    ""

# game/script.rpy:210
translate english T1_ac499e0c:

    # "But you still train regardless, but archive little."
    ""

# game/script.rpy:225
translate english Act2_d662fdb4:

    # y "It's the next day."
    y ""

# game/script.rpy:226
translate english Act2_658522d4:

    # y "I don't recall I have much sleep yesterday, but still, I feel refresh like every morning before."
    y ""

# game/script.rpy:227
translate english Act2_1bba7c0e:

    # y "Where should I go?"
    y ""

# game/script.rpy:241
translate english Act2market_ebfbd788:

    # y "(Out of the blue, I decided to visit the local market.)"
    y ""

# game/script.rpy:242
translate english Act2market_6f0cc17f:

    # y "(It's only a walk away, but it's a crowded spot for people to buy stuff.)"
    y ""

# game/script.rpy:243
translate english Act2market_a491077e:

    # y "(Things look normal until I saw someone creeping in from the fence.)"
    y ""

# game/script.rpy:244
translate english Act2market_cb72f9cd:

    # y "(There are five of them and all of them are masked.)"
    y ""

# game/script.rpy:245
translate english Act2market_8e744125:

    # y "(All of them have sharp knifes, and one of them even has a gun.)"
    y ""

# game/script.rpy:246
translate english Act2market_c746a7d8:

    # y "(Without thinking twice, I pass through the crowd, and enter the dark alleyway and approach them.)"
    y ""

# game/script.rpy:248
translate english Act2market_b4d67c84:

    # "Why is someone coming in?!"
    ""

# game/script.rpy:249
translate english Act2market_bf1b59c8:

    # "I don't know! Isn't suppose your power to avoid attention be in used?"
    ""

# game/script.rpy:250
translate english Act2market_240a29f3:

    # y "I see, you guys are up to not good."
    y ""

# game/script.rpy:254
translate english Act2market_47ea3df5:

    # u "Hah! it's only a small girl, bring her down!"
    u ""

# game/script.rpy:255
translate english Act2market_3569f2b2:

    # y "One of the bigger guy approach me, and trying to bring me down."
    y ""

# game/script.rpy:256
translate english Act2market_b89c2b6f:

    # "Your power is [power] against 5."
    ""

# game/script.rpy:259
translate english Act2market_68ba4049:

    # y "Hmm, not bad, quite a punch."
    y ""

# game/script.rpy:260
translate english Act2market_fc303a26:

    # "As the man punch down, [y] move slightly backwards."
    ""

# game/script.rpy:261
translate english Act2market_02c3ac16:

    # y "But it's not enough to bring me down."
    y ""

# game/script.rpy:262
translate english Act2market_772432fb:

    # y "(His hands look strong but not as strong as I expected.)"
    y ""

# game/script.rpy:263
translate english Act2market_1db46b19:

    # y "(A throw to the wall aside, I bring him down regardless.)"
    y ""

# game/script.rpy:266
translate english Act2market_7d5d900b:

    # y "Hmm."
    y ""

# game/script.rpy:267
translate english Act2market_91f20c0f:

    # "As the man punch down, [y] only slightly budged."
    ""

# game/script.rpy:268
translate english Act2market_1dd5d005:

    # y "A bit surprisingly."
    y ""

# game/script.rpy:269
translate english Act2market_6e328b9c:

    # y "(His hands are slow, so I don't see it as much threat.)"
    y ""

# game/script.rpy:270
translate english Act2market_8f38f158:

    # y "(I knocked him out easily, and now look at the rest grinning.)"
    y ""

# game/script.rpy:273
translate english Act2market_7d5d900b_1:

    # y "Hmm."
    y ""

# game/script.rpy:274
translate english Act2market_173f7324:

    # "As the man punch down, [y] isn't affected at all."
    ""

# game/script.rpy:275
translate english Act2market_230c2c16:

    # y "Really."
    y ""

# game/script.rpy:276
translate english Act2market_0ecd6393:

    # y "(His hands are slow, but I think I overestimate the impact.)"
    y ""

# game/script.rpy:277
translate english Act2market_8f38f158_1:

    # y "(I knocked him out easily, and now look at the rest grinning.)"
    y ""

# game/script.rpy:279
translate english Act2market_2ae92672:

    # u "Who are you!"
    u ""

# game/script.rpy:280
translate english Act2market_68b34139:

    # u "Why he can knocked out John so easily! I thought he is a rank C?"
    u ""

# game/script.rpy:281
translate english Act2market_23fe8681:

    # y "Hey, don't ignore me."
    y ""

# game/script.rpy:283
translate english Act2market_6383721d:

    # u "Yikes!"
    u ""

# game/script.rpy:284
translate english Act2market_5d4aa184:

    # "Masked men are down."
    ""

# game/script.rpy:285
translate english Act2market_e28686ed:

    # y "Should I call the cops now then...?"
    y ""

# game/script.rpy:290
translate english Act2garden_2be5dbd5:

    # y "(The nearby garden is a good place to get some refreshing air.)"
    y ""

# game/script.rpy:291
translate english Act2garden_d050e2c2:

    # y "(There is some people but only elderly and the young, with mothers taking care of their children.)"
    y ""

# game/script.rpy:292
translate english Act2garden_f4373da9:

    # y "(Things look normal until I saw the public toilet is surprisingly busy.)"
    y ""

# game/script.rpy:293
translate english Act2garden_cb72f9cd:

    # y "(There are five of them and all of them are masked.)"
    y ""

# game/script.rpy:294
translate english Act2garden_8e744125:

    # y "(All of them have sharp knifes, and one of them even has a gun.)"
    y ""

# game/script.rpy:295
translate english Act2garden_0714cd9a:

    # y "(Without thinking twice, I approach them as I just enter the male bathroom awkwardly.)"
    y ""

# game/script.rpy:296
translate english Act2garden_b4d67c84:

    # "Why is someone coming in?!"
    ""

# game/script.rpy:297
translate english Act2garden_bf1b59c8:

    # "I don't know! Isn't suppose your power to avoid attention be in used?"
    ""

# game/script.rpy:298
translate english Act2garden_240a29f3:

    # y "I see, you guys are up to not good."
    y ""

# game/script.rpy:302
translate english Act2garden_47ea3df5:

    # u "Hah! it's only a small girl, bring her down!"
    u ""

# game/script.rpy:303
translate english Act2garden_6a1a1439:

    # y "One of the helmet guy approach me, and he is using brainwashing power."
    y ""

# game/script.rpy:304
translate english Act2garden_b89c2b6f:

    # "Your power is [power] against 5."
    ""

# game/script.rpy:307
translate english Act2garden_e829e201:

    # y "Hmm, not bad, but still far from effective."
    y ""

# game/script.rpy:308
translate english Act2garden_56f6eb39:

    # "As the man fail to brainwash, [y] move slightly backwards."
    ""

# game/script.rpy:309
translate english Act2garden_02c3ac16:

    # y "But it's not enough to bring me down."
    y ""

# game/script.rpy:310
translate english Act2garden_b4515d7a:

    # y "(His power is strong but not as strong as I expected.)"
    y ""

# game/script.rpy:311
translate english Act2garden_1db46b19:

    # y "(A throw to the wall aside, I bring him down regardless.)"
    y ""

# game/script.rpy:314
translate english Act2garden_7d5d900b:

    # y "Hmm."
    y ""

# game/script.rpy:315
translate english Act2garden_20b57305:

    # "As the man fail to brainwash, [y] only slightly budged."
    ""

# game/script.rpy:316
translate english Act2garden_1dd5d005:

    # y "A bit surprisingly."
    y ""

# game/script.rpy:317
translate english Act2garden_d91e1a7b:

    # y "(His power is slow, so I don't see it as much threat.)"
    y ""

# game/script.rpy:318
translate english Act2garden_8f38f158:

    # y "(I knocked him out easily, and now look at the rest grinning.)"
    y ""

# game/script.rpy:321
translate english Act2garden_7d5d900b_1:

    # y "Hmm."
    y ""

# game/script.rpy:322
translate english Act2garden_45af8ebd:

    # "As the man fail to brainwash, [y] isn't affected at all."
    ""

# game/script.rpy:323
translate english Act2garden_230c2c16:

    # y "Really."
    y ""

# game/script.rpy:324
translate english Act2garden_91d67633:

    # y "(His power are slow, but I still think I overestimate the impact.)"
    y ""

# game/script.rpy:325
translate english Act2garden_8f38f158_1:

    # y "(I knocked him out easily, and now look at the rest grinning.)"
    y ""

# game/script.rpy:327
translate english Act2garden_2ae92672:

    # u "Who are you!"
    u ""

# game/script.rpy:328
translate english Act2garden_bb0beecc:

    # u "Why he can knocked out Ken so easily! I thought he is a rank C?"
    u ""

# game/script.rpy:329
translate english Act2garden_23fe8681:

    # y "Hey, don't ignore me."
    y ""

# game/script.rpy:331
translate english Act2garden_6383721d:

    # u "Yikes!"
    u ""

# game/script.rpy:332
translate english Act2garden_5d4aa184:

    # "Masked men are down."
    ""

# game/script.rpy:333
translate english Act2garden_e28686ed:

    # y "Should I call the cops now then...?"
    y ""

# game/script.rpy:334
translate english Act2garden_7fdf7857:

    # y "It's quite a mess..."
    y ""

# game/script.rpy:338
translate english Act2metro_de128d3e:

    # y "(To get to Raven, I have to get into the metro, where I saw some supicious people walking behind alleyway.)"
    y ""

# game/script.rpy:339
translate english Act2metro_da77ded2:

    # y "(I begin to follow them.)"
    y ""

# game/script.rpy:340
translate english Act2metro_cb72f9cd:

    # y "(There are five of them and all of them are masked.)"
    y ""

# game/script.rpy:341
translate english Act2metro_bc16b46c:

    # y "(I can see them taking direction from a device before destroyed them on ground, looks like a power device too.)"
    y ""

# game/script.rpy:342
translate english Act2metro_14e98b02:

    # y "(All of them have gun, and one of them even has a shield.)"
    y ""

# game/script.rpy:343
translate english Act2metro_0714cd9a:

    # y "(Without thinking twice, I approach them as I just enter the male bathroom awkwardly.)"
    y ""

# game/script.rpy:345
translate english Act2metro_b4d67c84:

    # "Why is someone coming in?!"
    ""

# game/script.rpy:346
translate english Act2metro_bf1b59c8:

    # "I don't know! Isn't suppose your power to avoid attention be in used?"
    ""

# game/script.rpy:347
translate english Act2metro_240a29f3:

    # y "I see, you guys are up to not good."
    y ""

# game/script.rpy:351
translate english Act2metro_47ea3df5:

    # u "Hah! it's only a small girl, bring her down!"
    u ""

# game/script.rpy:352
translate english Act2metro_6a1a1439:

    # y "One of the helmet guy approach me, and he is using brainwashing power."
    y ""

# game/script.rpy:353
translate english Act2metro_361c592a:

    # "Your power is [power] against 15."
    ""

# game/script.rpy:356
translate english Act2metro_baebef0c:

    # y "Hmm... quite a power."
    y ""

# game/script.rpy:357
translate english Act2metro_ed2e8c50:

    # "As the man trying to control over her, [y] move backwards effected by the impact."
    ""

# game/script.rpy:358
translate english Act2metro_c177bf2a:

    # y "But it's not enough to bring me down!"
    y ""

# game/script.rpy:359
translate english Act2metro_4ad55ee4:

    # y "(His power look strong and are as strong as I expected.)"
    y ""

# game/script.rpy:360
translate english Act2metro_c14042ea:

    # y "(A tought struggle, but I bring him down in the end.)"
    y ""

# game/script.rpy:363
translate english Act2metro_e829e201:

    # y "Hmm, not bad, but still far from effective."
    y ""

# game/script.rpy:364
translate english Act2metro_56f6eb39:

    # "As the man fail to brainwash, [y] move slightly backwards."
    ""

# game/script.rpy:365
translate english Act2metro_02c3ac16:

    # y "But it's not enough to bring me down."
    y ""

# game/script.rpy:366
translate english Act2metro_b4515d7a:

    # y "(His power is strong but not as strong as I expected.)"
    y ""

# game/script.rpy:367
translate english Act2metro_1db46b19:

    # y "(A throw to the wall aside, I bring him down regardless.)"
    y ""

# game/script.rpy:370
translate english Act2metro_7d5d900b:

    # y "Hmm."
    y ""

# game/script.rpy:371
translate english Act2metro_20b57305:

    # "As the man fail to brainwash, [y] only slightly budged."
    ""

# game/script.rpy:372
translate english Act2metro_1dd5d005:

    # y "A bit surprisingly."
    y ""

# game/script.rpy:373
translate english Act2metro_d91e1a7b:

    # y "(His power is slow, so I don't see it as much threat.)"
    y ""

# game/script.rpy:374
translate english Act2metro_8f38f158:

    # y "(I knocked him out easily, and now look at the rest grinning.)"
    y ""

# game/script.rpy:377
translate english Act2metro_7d5d900b_1:

    # y "Hmm."
    y ""

# game/script.rpy:378
translate english Act2metro_45af8ebd:

    # "As the man fail to brainwash, [y] isn't affected at all."
    ""

# game/script.rpy:379
translate english Act2metro_230c2c16:

    # y "Really."
    y ""

# game/script.rpy:380
translate english Act2metro_91d67633:

    # y "(His power are slow, but I still think I overestimate the impact.)"
    y ""

# game/script.rpy:381
translate english Act2metro_8f38f158_1:

    # y "(I knocked him out easily, and now look at the rest grinning.)"
    y ""

# game/script.rpy:383
translate english Act2metro_bb0beecc:

    # u "Why he can knocked out Ken so easily! I thought he is a rank C?"
    u ""

# game/script.rpy:384
translate english Act2metro_79f5d904:

    # u "Dude, go!"
    u ""

# game/script.rpy:385
translate english Act2metro_361c592a_1:

    # "Your power is [power] against 15."
    ""

# game/script.rpy:388
translate english Act2metro_66b8342e:

    # y "Hmm... quite a punch."
    y ""

# game/script.rpy:389
translate english Act2metro_9fc50a01:

    # "As the man punch down, [y] move backwards effected by the impact."
    ""

# game/script.rpy:390
translate english Act2metro_c177bf2a_1:

    # y "But it's not enough to bring me down!"
    y ""

# game/script.rpy:391
translate english Act2metro_49af1699:

    # y "(His hands look strong and are as strong as I expected.)"
    y ""

# game/script.rpy:392
translate english Act2metro_61feafcd:

    # y "(A tought fight, but I bring him down regardless.)"
    y ""

# game/script.rpy:395
translate english Act2metro_68ba4049:

    # y "Hmm, not bad, quite a punch."
    y ""

# game/script.rpy:396
translate english Act2metro_fc303a26:

    # "As the man punch down, [y] move slightly backwards."
    ""

# game/script.rpy:397
translate english Act2metro_02c3ac16_1:

    # y "But it's not enough to bring me down."
    y ""

# game/script.rpy:398
translate english Act2metro_772432fb:

    # y "(His hands look strong but not as strong as I expected.)"
    y ""

# game/script.rpy:399
translate english Act2metro_1db46b19_1:

    # y "(A throw to the wall aside, I bring him down regardless.)"
    y ""

# game/script.rpy:402
translate english Act2metro_7d5d900b_2:

    # y "Hmm."
    y ""

# game/script.rpy:403
translate english Act2metro_91f20c0f:

    # "As the man punch down, [y] only slightly budged."
    ""

# game/script.rpy:404
translate english Act2metro_1dd5d005_1:

    # y "A bit surprisingly."
    y ""

# game/script.rpy:405
translate english Act2metro_6e328b9c:

    # y "(His hands are slow, so I don't see it as much threat.)"
    y ""

# game/script.rpy:406
translate english Act2metro_8f38f158_2:

    # y "(I knocked him out easily, and now look at the rest grinning.)"
    y ""

# game/script.rpy:409
translate english Act2metro_7d5d900b_3:

    # y "Hmm."
    y ""

# game/script.rpy:410
translate english Act2metro_173f7324:

    # "As the man punch down, [y] isn't affected at all."
    ""

# game/script.rpy:411
translate english Act2metro_230c2c16_1:

    # y "Really."
    y ""

# game/script.rpy:412
translate english Act2metro_0ecd6393:

    # y "(His hands are slow, but I think I overestimate the impact.)"
    y ""

# game/script.rpy:413
translate english Act2metro_8f38f158_3:

    # y "(I knocked him out easily, and now look at the rest grinning.)"
    y ""

# game/script.rpy:415
translate english Act2metro_2ae92672:

    # u "Who are you!"
    u ""

# game/script.rpy:416
translate english Act2metro_c12c6222:

    # u "Shoot at her!"
    u ""

# game/script.rpy:417
translate english Act2metro_5b723dee:

    # y "Hey, don't mess things up!"
    y ""

# game/script.rpy:419
translate english Act2metro_6383721d:

    # u "Yikes!"
    u ""

# game/script.rpy:420
translate english Act2metro_5d4aa184:

    # "Masked men are down."
    ""

# game/script.rpy:421
translate english Act2metro_e28686ed:

    # y "Should I call the cops now then...?"
    y ""

# game/script.rpy:428
translate english Act2B_30864be8:

    # y "Oh well."
    y ""

# game/script.rpy:429
translate english Act2B_37096590:

    # y "We are back here again, aren't we?"
    y ""

# game/script.rpy:430
translate english Act2B_ea413d33:

    # v "Yes my goddess."
    v ""

# game/script.rpy:431
translate english Act2B_40959908:

    # v "How's things down there?"
    v ""

# game/script.rpy:432
translate english Act2B_e9f885c7:

    # y "Good? Aren't I suppose to be with Raven now?"
    y ""

# game/script.rpy:433
translate english Act2B_fb7f0698:

    # v "Yes, there will be nothing change unless you want to."
    v ""

# game/script.rpy:434
translate english Act2B_1cdb7ae5:

    # y "Then why am I here?"
    y ""

# game/script.rpy:435
translate english Act2B_3e80ad4f:

    # v "Couple of reasons."
    v ""

# game/script.rpy:436
translate english Act2B_a9a7e4f9:

    # v "There's a change of things after you engage in the criminal affairs."
    v ""

# game/script.rpy:437
translate english Act2B_c80af550:

    # v "Hence, you are here to be told of such thing."
    v ""

# game/script.rpy:438
translate english Act2B_138eba0a:

    # y "I see."
    y ""

# game/script.rpy:439
translate english Act2B_e9d8c3a7:

    # y "What's the other reasons?"
    y ""

# game/script.rpy:440
translate english Act2B_e0d9b6b3:

    # v "I thought it's about Raven?"
    v ""

# game/script.rpy:441
translate english Act2B_71a86863:

    # y "..."
    y ""

# game/script.rpy:442
translate english Act2B_63cf1c4b:

    # y "Well, I think I like her, so yeah."
    y ""

# game/script.rpy:443
translate english Act2B_7e149412:

    # v "Do you want her to be special?"
    v ""

# game/script.rpy:444
translate english Act2B_7ce38287:

    # v "The world is at your will to change."
    v ""

# game/script.rpy:445
translate english Act2B_71a86863_1:

    # y "..."
    y ""

# game/script.rpy:446
translate english Act2B_4f587f26:

    # v "You don't seem conviced."
    v ""

# game/script.rpy:447
translate english Act2B_ba84b3b8:

    # y "I would like to help her, but also I didn't want to just ignore her past hard work."
    y ""

# game/script.rpy:448
translate english Act2B_65cabcbf:

    # v "..."
    v ""

# game/script.rpy:449
translate english Act2B_e4fa54cd:

    # v "It's alright, nothing will change if you say so."
    v ""

# game/script.rpy:450
translate english Act2B_28bcaf26:

    # v "You have the final call."
    v ""

# game/script.rpy:453
translate english Act2B_574e77f3:

    # y "(I wasn't so sure, but I did change some thing.)"
    y ""

# game/script.rpy:454
translate english Act2B_938c9ea8:

    # y "(As well as seeing some possible future... I don't know what I am feeling right now.)"
    y ""

# game/script.rpy:455
translate english Act2B_7b7f2258:

    # y "(Except for this is how god would feel, emptyness and peace for strange reasons.)"
    y ""

# game/script.rpy:457
translate english Act2B_1cfa5cc2:

    # r "Hi [y]! I come to see you today!"
    r ""

# game/script.rpy:458
translate english Act2B_177422a3:

    # y "Hi Raven, wow it's still early in the morning, did something nice happen?"
    y ""

# game/script.rpy:459
translate english Act2B_6b64c0a0:

    # r "Well yeah! I got a position in the Hero Association after help defeating the mysterious super gang in the street!"
    r ""

# game/script.rpy:460
translate english Act2B_e7b57619:

    # r "One of them tried to mind control me, but his power is too weak, HA!"
    r ""

# game/script.rpy:461
translate english Act2B_5284db20:

    # y "That's great! But take care will you, it seems like the new criminal gang is really hard to deal with."
    y ""

# game/script.rpy:462
translate english Act2B_130feeee:

    # r "Yeah about that, my face has been broadcasted city wide last night..."
    r ""

# game/script.rpy:463
translate english Act2B_8928aa04:

    # r "Can I hide here for the time being?"
    r ""

# game/script.rpy:464
translate english Act2B_0fc4ec84:

    # y "Yesterday?"
    y ""

# game/script.rpy:465
translate english Act2B_9d4b7dd9:

    # r "Yes, well... it's a breaking news thing, youngest like us don't watch TV nowdays."
    r ""

# game/script.rpy:466
translate english Act2B_b42c80ac:

    # y "Ah I scare if I miss something haha... well why the sudden hiding?"
    y ""

# game/script.rpy:467
translate english Act2B_786210f9:

    # r "A lot of people know me in that area... you know, like..."
    r ""

# game/script.rpy:468
translate english Act2B_95abe705:

    # r "I am a three time karate champion or something."
    r ""

# game/script.rpy:469
translate english Act2B_38673c5f:

    # r "Now I am recruited to be a heroine... soon, I got more attention than usual."
    r ""

# game/script.rpy:470
translate english Act2B_77d2193a:

    # y "I thought you used to that by now."
    y ""

# game/script.rpy:471
translate english Act2B_3cb2288c:

    # r "Yes but no..."
    r ""

# game/script.rpy:472
translate english Act2B_bf9e5b76:

    # y "(Raven blushed face is kinda cute like always.)"
    y ""

# game/script.rpy:473
translate english Act2B_809d1816:

    # r "Anyways, I just want to stay out of sight until the heats go down."
    r ""

# game/script.rpy:474
translate english Act2B_0cc7ba0d:

    # y "No problem Raven, you can stay here as long as you want."
    y ""

# game/script.rpy:475
translate english Act2B_f57aabaa:

    # y "(I wonder how Raven's stats is.)"
    y ""

# game/script.rpy:476
translate english Act2B_19b5e0cd:

    # "Raven power stats 5000, power stats 0"
    ""

# game/script.rpy:477
translate english Act2B_c869c832:

    # y "(Wow she's already that strong!)"
    y ""

# game/script.rpy:478
translate english Act2B_52bc3748:

    # y "(With that strength, she can beat most of the heroes out there.)"
    y ""

# game/script.rpy:479
translate english Act2B_8f692d29:

    # r "Is there something on my face?"
    r ""

# game/script.rpy:480
translate english Act2B_c5fe3fdc:

    # y "Hmm, I just think you are cute hehe."
    y ""

# game/script.rpy:481
translate english Act2B_c11e135f:

    # r "Oh come on [y]."
    r ""

# game/script.rpy:482
translate english Act2B_91aa5eca:

    # y "Hehe."
    y ""

# game/script.rpy:485
translate english Act2B_fd8a795f:

    # y "(I thought it's not going to be problematic, maybe.)"
    y ""

# game/script.rpy:486
translate english Act2B_43af8369:

    # y "(But for some reason, I have to enlist myself in the Hero Asociation thing for being close to Raven.)"
    y ""

# game/script.rpy:487
translate english Act2B_9deea1ae:

    # r "(Sorry [y] hehe, it seems I have to list my love ones so the bad guys won't target you and my family, hopefully.)"
    r ""

# game/script.rpy:488
translate english Act2B_a27f1f47:

    # y "I guess it will be a fast ride."
    y ""

# game/script.rpy:491
translate english Act2B_2f6aed77:

    # s1 "Please wait here, [y]."
    s1 ""

# game/script.rpy:492
translate english Act2B_a86a703c:

    # y "Sure..."
    y ""

# game/script.rpy:493
translate english Act2B_0ec57fba:

    # y "(Hopefully it's nothing.)"
    y ""

# game/script.rpy:494
translate english Act2B_6465145d:

    # "A few minutes later."
    ""

# game/script.rpy:495
translate english Act2B_893d1397:

    # qq "Hi nice to meet you, [y]"
    qq ""

# game/script.rpy:496
translate english Act2B_cb993cb1:

    # y "Hi sir, nice to meet you too."
    y ""

# game/script.rpy:497
translate english Act2B_280b9e8e:

    # a "Hi my name is Ace, the former head for the Hero Association!"
    a ""

# game/script.rpy:498
translate english Act2B_64cdf871:

    # y "Yes! I have heard of your stories Mr Ace! It's a pleasure to meet you!"
    y ""

# game/script.rpy:499
translate english Act2B_11c7c246:

    # a "Sorry for my personal request, I need to have few words with you, young girl."
    a ""

# game/script.rpy:500
translate english Act2B_dc9882eb:

    # "Mr Ace signal the room to be clear as the receptionist gets out."
    ""

# game/script.rpy:501
translate english Act2B_62802bf9:

    # a "As you know, Raven is our talented girl that we haven't seen for years."
    a ""

# game/script.rpy:502
translate english Act2B_b8d57d88:

    # a "She is as strong as our top hero now, The Justice Man, and she is still young in her power."
    a ""

# game/script.rpy:503
translate english Act2B_cd970f8d:

    # a "As as her friend, you! We like to make sure she didn't end up getting hurt by the bad guys alike, or any bad influences..."
    a ""

# game/script.rpy:504
translate english Act2B_755058e1:

    # y "Ok Mr Ace..."
    y ""

# game/script.rpy:505
translate english Act2B_17236b5e:

    # y "(It seems he's going to talk for a long time.)"
    y ""

# game/script.rpy:506
translate english Act2B_85bd71f0:

    # "Hours later."
    ""

# game/script.rpy:507
translate english Act2B_fa02fb29:

    # a "Sorry I keep you for this long haha! It's uncommon for people to listen to me this long."
    a ""

# game/script.rpy:508
translate english Act2B_c69d019c:

    # y "It's ok sir, I know."
    y ""

# game/script.rpy:509
translate english Act2B_da0e3329:

    # a "Since [y] you are a super yourself, are you interested in growing your own power?"
    a ""

# game/script.rpy:510
translate english Act2B_71a86863_2:

    # y "..."
    y ""

# game/script.rpy:511
translate english Act2B_28cf499d:

    # a "Ah, I know not eveyone wants to be a hero or such, but you know, it can help you if you want to."
    a ""

# game/script.rpy:512
translate english Act2B_9d572008:

    # y "Sure? Thank you Mr Ace."
    y ""

# game/script.rpy:513
translate english Act2B_668d348f:

    # a "No problem!"
    a ""

# game/script.rpy:514
translate english Act2B_6d458645:

    # y "(Mr Ace is a nice guy, but he is too talkactive and it rubs people in the wrong way.)"
    y ""

# game/script.rpy:517
translate english Act2B_7b5bd62b:

    # y "(I need to be stronger too, Raven seems to be stronger than me at the moment.)"
    y ""

# game/script.rpy:518
translate english Act2B_a8446644:

    # "A stronger floor and a unique space was made for training in the room."
    ""

# game/script.rpy:534
translate english Stat2_1cc1d4da:

    # "Your power is [power]"
    ""

# game/script.rpy:542
translate english T3_fb9ef9d0:

    # "You plan to lift 1 million tons."
    ""

# game/script.rpy:543
translate english T3_452d9257:

    # "Almost a big building and 1/6 of a pryamid would weight."
    ""

# game/script.rpy:544
translate english T3_e2ffc2bc:

    # "You feel like your body is chilling at best."
    ""

# game/script.rpy:545
translate english T3_bf3654ed:

    # "As you move the weight, in a shape of dumbbells made by your goddess creation power."
    ""

# game/script.rpy:546
translate english T3_f89b28d3:

    # "You lift it once, and you already feel your body in full emerged in power."
    ""

# game/script.rpy:550
translate english T3_f7fc1ae1:

    # "Next is 10 million tons."
    ""

# game/script.rpy:551
translate english T3_4eb92243:

    # "It's almost the weight of a city."
    ""

# game/script.rpy:552
translate english T3_9114431e:

    # "It's the same shape of dumbbells, and the floor is clearing crying."
    ""

# game/script.rpy:553
translate english T3_22cd0fa5:

    # "You lift it carefully, almost trying to feel it's power."
    ""

# game/script.rpy:554
translate english T3_86bc3f90:

    # "You feel unchallenged, and feeling like your body is teasing for more."
    ""

# game/script.rpy:558
translate english T3_f729803b:

    # "100 trillion tons sound a lot, but you know you can do more."
    ""

# game/script.rpy:559
translate english T3_441082a1:

    # "This time it's in the shape of barbells, because you want to view it as 'heavy' maybe."
    ""

# game/script.rpy:560
translate english T3_7b12ede0:

    # "It feels as easy as before, only this time, the ground can't withstand and collapsed a bit, even though you reinforced it more."
    ""

# game/script.rpy:561
translate english T3_15add80b:

    # "You quickly fix it before things become out of hand, and the weight is already too little to be notice."
    ""

# game/script.rpy:565
translate english T3_9e4db324:

    # "There's no heavy enough equipment for you to train."
    ""

# game/script.rpy:566
translate english T3_03f44552:

    # "You try to imagine something heavy on your hands, and then lift it once."
    ""

# game/script.rpy:567
translate english T3_952cef79:

    # "Although archive little, it's still much more than most can do."
    ""

# game/script.rpy:580
translate english Act3_1cc1d4da:

    # "Your power is [power]"
    ""

# game/script.rpy:581
translate english Act3_b6a760f1:

    # y "It's time to see how am I doing against Raven..."
    y ""

# game/script.rpy:582
translate english Act3_c14dd4a8:

    # "Raven power stats 10000"
    ""

# game/script.rpy:585
translate english Act3_aee1432b:

    # y "(Wow! She grows so fast...)"
    y ""

# game/script.rpy:586
translate english Act3_a60d96f4:

    # y "I must become stronger than her!"
    y ""

# game/script.rpy:587
translate english Act3_b5d6eee2:

    # y "No, I'm not jealous!"
    y ""

# game/script.rpy:588
translate english Act3_2789a893:

    # y "Go back to last night!"
    y ""

# game/script.rpy:592
translate english Act3_aac22780:

    # y "(It's close...)"
    y ""

# game/script.rpy:593
translate english Act3_e1074350:

    # y "Do I need to train more?"
    y ""

# game/script.rpy:602
translate english Act3_7d48c4df:

    # y "Raven might be strong, but I am stronger!"
    y ""

# game/script.rpy:606
translate english Act3Down_a5cd8a81:

    # r "Hi [y]."
    r ""

# game/script.rpy:607
translate english Act3Down_6fbca073:

    # "Raven appears at your door."
    ""

# game/script.rpy:608
translate english Act3Down_8c83846f:

    # r "Come out now! I can hear you in there!"
    r ""

# game/script.rpy:609
translate english Act3Down_7789c3eb:

    # y "(Yikes! Does she listen to what I say just now?)"
    y ""

# game/script.rpy:610
translate english Act3Down_c542689f:

    # y "Hi Raven, you are earlier than last time, haha..."
    y ""

# game/script.rpy:611
translate english Act3Down_4af2ec06:

    # r "Yes! And I want to tell you that I get really strong!"
    r ""

# game/script.rpy:612
translate english Act3Down_c8052a6b:

    # r "Now I am at least a billion times stronger than Justice Man! You can't believe it!"
    r ""

# game/script.rpy:613
translate english Act3Down_30495171:

    # y "Yes!"
    y ""

# game/script.rpy:614
translate english Act3Down_44b72828:

    # r "Huh?"
    r ""

# game/script.rpy:615
translate english Act3Down_0fa6e40f:

    # y "Oh I mean, I didn't believe it...?"
    y ""

# game/script.rpy:616
translate english Act3Down_95e85f1c:

    # r "Hehe [y], I know if you hide something from me..."
    r ""

# game/script.rpy:618
translate english Act3Down_3dda19ac:

    # "Raven push you to bed."
    ""

# game/script.rpy:620
translate english Act3Down_c8061262:

    # y "Raven?"
    y ""

# game/script.rpy:621
translate english Act3Down_ab3e5543:

    # r "I heard those things you said last night, my training has able me to hear everything this planet has to say."
    r ""

# game/script.rpy:622
translate english Act3Down_28fd4457:

    # r "You are stronger than me, don't you."
    r ""

# game/script.rpy:633
translate english Act3r1_21402e22:

    # "For some reason, Raven looks mad."
    ""

# game/script.rpy:634
translate english Act3r1_c02e063d:

    # r "You lied."
    r ""

# game/script.rpy:635
translate english Act3r1_a3612005:

    # y "You don't understand! I made you to become..."
    y ""

# game/script.rpy:636
translate english Act3r1_908046c0:

    # y "I'm sorry Raven."
    y ""

# game/script.rpy:637
translate english Act3r1_495b8d8c:

    # r "Why."
    r ""

# game/script.rpy:638
translate english Act3r1_85130922:

    # y "(Raven eyes look so sad, I don't even know how to respond.)"
    y ""

# game/script.rpy:639
translate english Act3r1_e436b103:

    # r "It's ok [y], we can do this next time."
    r ""

# game/script.rpy:640
translate english Act3r1_8429f375:

    # y "(As weird as it seen, Raven and I both undress ourselves, we have a long and satisfying sex.)"
    y ""

# game/script.rpy:641
translate english Act3r1_f3984662:

    # y "(Her power constantly overpowered me, as I can feel her hard breasts and nipples shoving into me.)"
    y ""

# game/script.rpy:642
translate english Act3r1_3f278518:

    # y "(Her tongue is soft yet aggressive, unable to tell wheter or not if she's serious.)"
    y ""

# game/script.rpy:643
translate english Act3r1_3ae3bc67:

    # y "(Raven ultimately overpowered me completely as she hug and squeeze me with more strength.)"
    y ""

# game/script.rpy:644
translate english Act3r1_4d2ebc84:

    # y "(I could feel like my body is trying to grow, but nothing can stop Raven's power now...)"
    y ""

# game/script.rpy:645
translate english Act3r1_3e328fbd:

    # y "(How can it be?)"
    y ""

# game/script.rpy:646
translate english Act3r1_71a86863:

    # y "..."
    y ""

# game/script.rpy:647
translate english Act3r1_3237b72b:

    # "The End of Raven Ending."
    ""

# game/script.rpy:653
translate english Act3r2_224a45db:

    # y "(Raven and I stare at each other for a minute.)"
    y ""

# game/script.rpy:654
translate english Act3r2_8fa31a09:

    # "Raven slightly move away."
    ""

# game/script.rpy:655
translate english Act3r2_740d94e3:

    # r "I always like you [y]."
    r ""

# game/script.rpy:656
translate english Act3r2_89ef9187:

    # r "But lets see how much you got!"
    r ""

# game/script.rpy:658
translate english Act3r2_5ef37db4:

    # "The world changed as both girls are inside a special world out of any realm and existence."
    ""

# game/script.rpy:659
translate english Act3r2_83319c0d:

    # "Raven power stats 10000000, power stats 10000000"
    ""

# game/script.rpy:661
translate english Act3r2_df959266:

    # r "Too weak!"
    r ""

# game/script.rpy:662
translate english Act3r2_c6f63094:

    # y "Urgh!"
    y ""

# game/script.rpy:663
translate english Act3r2_52c8fd24:

    # r "[y] I thought you want to be strong? Did you forget our promise?"
    r ""

# game/script.rpy:664
translate english Act3r2_f9e5ad39:

    # y "Not so much, Hah!"
    y ""

# game/script.rpy:665
translate english Act3r2_82b79fb6:

    # y "(I try to jump and rush myself to Raven after being struck into one of the planet, it still hurts on my back.)"
    y ""

# game/script.rpy:666
translate english Act3r2_67175089:

    # r "Too bad this is the best you can do!"
    r ""

# game/script.rpy:667
translate english Act3r2_35026c0d:

    # "Raven grows her power!"
    ""

# game/script.rpy:668
translate english Act3r2_79195ca9:

    # "Raven power stats 100000000, power stats 100000000"
    ""

# game/script.rpy:669
translate english Act3r2_c744158d:

    # r "I go easy on you, I become ten times stronger!"
    r ""

# game/script.rpy:670
translate english Act3r2_a7da1195:

    # y "Waah! Wait!"
    y ""

# game/script.rpy:672
translate english Act3r2_677ecf03:

    # "A line of planets are destroyed."
    ""

# game/script.rpy:673
translate english Act3r2_d502a780:

    # r "Hmm, that's a bit too weak."
    r ""

# game/script.rpy:674
translate english Act3r2_31e96006:

    # y "Hey! I still here!"
    y ""

# game/script.rpy:675
translate english Act3r2_24a60e06:

    # "Attacks on Raven are uneffective."
    ""

# game/script.rpy:676
translate english Act3r2_2d86e24b:

    # r "Cute! I like you [y]!"
    r ""

# game/script.rpy:677
translate english Act3r2_eb7bc7c0:

    # y "Hehe, I never felt so excited!"
    y ""

# game/script.rpy:679
translate english Act3r2_ed041348:

    # "Both girls battle for endless time to come."
    ""

# game/script.rpy:680
translate english Act3r2_d5b8b521:

    # "With no winners or losers, and without the constraint of time."
    ""

# game/script.rpy:681
translate english Act3r2_1ed7d634:

    # "It become endless in this realm, forgotten and slept..."
    ""

# game/script.rpy:682
translate english Act3r2_0f8a019e:

    # "Constant War Ending."
    ""

# game/script.rpy:686
translate english Act3r2_152f717b:

    # r "You are strong, [y]."
    r ""

# game/script.rpy:687
translate english Act3r2_a9103dc8:

    # y "(I grab Raven from behind as I push her with my hard and soft breasts.)"
    y ""

# game/script.rpy:688
translate english Act3r2_fe7138f8:

    # r "You are also really good at thiiiiS!"
    r ""

# game/script.rpy:690
translate english Act3r2_12c68c19:

    # "Planets are destroyed but Raven is unharm."
    ""

# game/script.rpy:691
translate english Act3r2_dbec0e62:

    # r "Hah, let me see if you worth it!"
    r ""

# game/script.rpy:692
translate english Act3r2_35026c0d_1:

    # "Raven grows her power!"
    ""

# game/script.rpy:693
translate english Act3r2_11253f02:

    # "Raven power stats 1000000000, power stats 1000000000"
    ""

# game/script.rpy:694
translate english Act3r2_fc80ccc1:

    # r "I have to become stronger, a hundred times stronger!"
    r ""

# game/script.rpy:695
translate english Act3r2_c3509e95:

    # y "I can do the same and more too!"
    y ""

# game/script.rpy:696
translate english Act3r2_6ace86e3:

    # r "Urgh!"
    r ""

# game/script.rpy:698
translate english Act3r2_ecf47067:

    # y "(The realm gets destroyed as I powered up more.)"
    y ""

# game/script.rpy:699
translate english Act3r2_48e5069b:

    # y "(Raven sees she can't catch up, accept her defeat.)"
    y ""

# game/script.rpy:700
translate english Act3r2_2ec7fc48:

    # r "You are awesome!"
    r ""

# game/script.rpy:701
translate english Act3r2_0407d59e:

    # r "You are making my body so sensitive!"
    r ""

# game/script.rpy:702
translate english Act3r2_7a1537cf:

    # y "I don't mind to have some fun with you..."
    y ""

# game/script.rpy:703
translate english Act3r2_20dd0a07:

    # y "(As I put my breasts on Raven, I could feel the cracking of her bones.)"
    y ""

# game/script.rpy:704
translate english Act3r2_f16bef28:

    # y "Not even your black hole proof body can do anything against me."
    y ""

# game/script.rpy:705
translate english Act3r2_7474d533:

    # r "Yes! MORE!"
    r ""

# game/script.rpy:706
translate english Act3r2_07c608d5:

    # y "I love you Raven!"
    y ""

# game/script.rpy:707
translate english Act3r2_b4dcc518:

    # "Sophisticate Love Ending."
    ""

translate english strings:

    # game/script.rpy:69
    old "That's weird, am I the new goddess?"
    new ""

    # game/script.rpy:69
    old "I am in doubt of you."
    new ""

    # game/script.rpy:168
    old "Let's train."
    new ""

    # game/script.rpy:168
    old "Show me my stats."
    new ""

    # game/script.rpy:168
    old "I think I am done for today."
    new ""

    # game/script.rpy:228
    old "Local market."
    new ""

    # game/script.rpy:228
    old "Nearby garden."
    new ""

    # game/script.rpy:228
    old "See what's up with Raven."
    new ""

    # game/script.rpy:594
    old "Nah, it's going to be fine."
    new ""

    # game/script.rpy:594
    old "I will play safe."
    new ""

    # game/script.rpy:623
    old "No...?"
    new ""

    # game/script.rpy:623
    old "Yes, sorry Raven I lied to you."
    new ""

