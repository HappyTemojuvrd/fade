﻿#TOP

#Character
define a = Character("Ace")
define v = Character("Void")
define r = Character("Raven")
define u = Character("Masked man")
define s1 = Character("Receptionist Lady")
define qq = Character("?")

#Audio
define audio.voidmusic = "audio/Cosmos - Gaze.mp3"
define audio.schoolmusic = "audio/Lieder ohne Worte, Op. 19b No. 1 in E Major, Andante con moto, MWV U86.mp3"
define audio.calm = "audio/Lieder ohne Worte, Op. 62 No. 1 in G Major, Andante espressivo, MWV U185 (Live Recording).mp3"
define audio.battle = "audio/To Aru Kagaku no Railgun S OST - Frenda.mp3"
define audio.scare = "audio/To Aru Kagaku no Railgun S OST - Junsui yue no ayamachi.mp3"
define audio.warm = "audio/To Aru Kagaku no Railgun S OST - Tatta hitotsu no houhou.mp3"
define audio.battle2 = "audio/To Aru Kagaku no Railgun S OST - Saikyou wo shimesu mono.mp3"




#Picture
image bg pic1 = "pic1.jpg"
image bg pic2 = "pic2.jpg"
image bg pic3 = "pic3.jpg"
image bg pic4 = "pic4.jpg"
image bg pic5 = "pic5.jpg"
image bg pic6 = "pic6.jpg"
image bg pic7 = "pic7.jpg"
image bg pic7b = "pic7b.jpg"
image bg pic8 = "pic8.jpg"
image bg pic9 = "pic9.png"
image bg pic10 = "pic10.jpg"
image bg black-screen = "black-screen.jpg"
image bg pic11 = "pic11.jpg"
image bg pic12 = "pic12.jpeg"
image bg pic13 = "pic13.jpg"


# The game starts here.
#################################        
    #Chapter 1                  #
#################################

label start:
    
    v "It's quiet in here."
    v "Let's make this different."
    play music voidmusic
    
    $ y = renpy.input("What is your name?")
    if y == "":
        $ y ="Astra"
        

    v "Pleased to meet you, [y]."
    y "Yes?"
    v "You are the chosen one is this timeless realm of space."
    v "This is the void, it is also me, now, using a collective consciousness to communicating with you."
    y "I see."
    scene bg pic1 
    y "It doesn't look so empty in here."
    v "It's only empty when no one is looking."
    v "Like particles without the constraint of time, everthing can be timeless and spaceless as long as there's no need for existence."
    y "Are you god?"
    v "No, I just the world itself."
    v "Now ready to serve you, my new master."
    menu:
        "That's weird, am I the new goddess?":
            jump start1
        "I am in doubt of you.":
            jump start2

label start1:
v "Yes, [y]."
jump Act1

label start2:
v "I'm all yours, [y]."
jump Act1
    
    
label Act1:
    v "You should get stronger, meanwhile I will guard this place."
    y "You mean I should train?"
    v "Accurately, yes."
    scene bg pic2
    y "What is this blue world?"
    y "Wait, I can see everything on it."
    v "Your power should be surging soon, do you want to try?"
    y "For sure, I can already feel it."
    y "The many paths for ascension."
    stop music
    
    play music schoolmusic
    scene bg pic3 
    "Location unknown."
    "And we are done, have a good holiday."
    "YEAH!"
    r "Hi [y], what are you waiting for, let's go."
    y "(Raven, my friend, she is a girl with potential.)"
    y "Yes."
    
    scene bg pic4
    r "You look blur today, is something wrong?"
    y "It's fine, I just think of something lately."
    r "Well we are turning 17 after this holiday is over."
    r "I really do miss our time."
    y "You do, you won your third karate champion just last month."
    y "You really want to become a heroine, don't you?"
    r "Yeah, most probably, a little awkward for me is that I don't have any super natural powers."
    y "Well you are super strong."
    r "But I want cool powers! Like eyes laser, flight and such!"
    r "Anyways let's leave all those problems behind for now, right now, I just want to have fun with you."
    y "Sure."

    scene bg pic6
    y "(Lionstar Project.)"
    y "(Raven is one of the many supers in the acadmic city of Area 52)"
    y "(A disclosure area where supers from every place around the world, and some even from other worlds.)"
    y "(I myself is also consider a super, but I didn't exposure my real identity here to anyone yet.)"
    y "(So far things are looking good, and time spent with Raven always feel short for me.)"
    
    scene bg pic5
    r "Ah, it's already night now."
    y "Yes."
    r "Are you heading back soon?"
    y "No, I don't have any plans."
    r "I see."
    r "[y]."
    y "Yes?"
    r "Do you like me?"
    y "..."
    y "I do like you."
    r "Hmm, I don't know."
    r "I like you [y]."
    r "I don't want to leave you for sure."
    y "Are you worried about me?"
    r "..."
    y "Well I will always be here."
    r "Are you sure you can still be with me, while I go to the heroine academic?"
    y "Why not?"
    r "You are not as strong as I know!"
    r "I mean you may not make it."
    y "It's only a test that I need to do, it should be fine."
    r "If you say so..."
    y "(Raven still feels sad for some reason.)"
    y "Fine, I make a promise."
    y "I will be there with you."
    r "Really?"
    y "Really."
    r "I suppose I'm such a silly one to be worry about you."
    "Raven has left."
    y "Me too Raven."
    
    scene bg pic7
    y "(I should be preparing.)"
    y "(What training should I do?)"
    
    #Base stats
    $ power = 10
    $ power = 10
    
    jump Training1
    
label Training1:
    menu:
     
        "Let's train.":
            jump T1
            
        "Show me my stats.":
            jump Stat
        
        "I think I am done for today.":
            jump Act2

label Stat:
    "Your power is [power]"
    jump Training1
            
            
            
label T1:

    if power == 10:
        "You plan to lift 10 tons."
        "You feel like your body is chilling at best."
        "As you move the weight, in a shape of dumbbells made by your goddess creation power."
        "You lift it once, and you already feel your body in full emerged in power."
        $ power += 10
        jump Training1 
    if power == 20:
        "Next is 100 tons."
        "It's the same shape of dumbbells, and the floor is clearing crying."
        "You lift it carefully, almost trying to feel it's power."
        "So far, you feel unchallenged."
        $ power += 10
        jump Training1
    if power == 30:
        "1000 tons sound like much, but you know you can do more."
        "This time it's in the shape of barbells."
        "It feels as easy as before, only this time, the ground can't withstand and collapsed."
        "You quickly fix it before things become out of hand."
        $ power += 10
        jump Training1
    if power >= 40:
        "There's no heavy enough equipment for you to train."
        "But you still train regardless, but archive little."
        $ power += 1
        jump Training1

        
        
#################################        
    #Chapter 2                  #
#################################                                
        
label Act2:
    stop music
    play music calm
    
    scene bg pic7b
    y "It's the next day."
    y "I don't recall I have much sleep yesterday, but still, I feel refresh like every morning before."
    y "Where should I go?"
    menu:
        
        "Local market.":
            jump Act2market
            
        "Nearby garden.":
            jump Act2garden
            
        "See what's up with Raven.":
            jump Act2metro

label Act2market:
    scene bg pic10
    y "(Out of the blue, I decided to visit the local market.)"
    y "(It's only a walk away, but it's a crowded spot for people to buy stuff.)"
    y "(Things look normal until I saw someone creeping in from the fence.)"
    y "(There are five of them and all of them are masked.)"
    y "(All of them have sharp knifes, and one of them even has a gun.)"
    y "(Without thinking twice, I pass through the crowd, and enter the dark alleyway and approach them.)"
    scene bg pic8
    "Why is someone coming in?!"
    "I don't know! Isn't suppose your power to avoid attention be in used?"
    y "I see, you guys are up to not good."
    stop music 
    play music battle
    scene bg pic8 with hpunch
    u "Hah! it's only a small girl, bring her down!"
    y "One of the bigger guy approach me, and trying to bring me down."
    "Your power is [power] against 5."
    
    if power <= 20:
        y "Hmm, not bad, quite a punch."
        "As the man punch down, [y] move slightly backwards."
        y "But it's not enough to bring me down."
        y "(His hands look strong but not as strong as I expected.)"
        y "(A throw to the wall aside, I bring him down regardless.)"
        
    if power <= 40:
        y "Hmm."
        "As the man punch down, [y] only slightly budged."
        y "A bit surprisingly."
        y "(His hands are slow, so I don't see it as much threat.)"
        y "(I knocked him out easily, and now look at the rest grinning.)"
    
    if power > 41:
        y "Hmm."
        "As the man punch down, [y] isn't affected at all."
        y "Really."
        y "(His hands are slow, but I think I overestimate the impact.)"
        y "(I knocked him out easily, and now look at the rest grinning.)"
    
    u "Who are you!"
    u "Why he can knocked out John so easily! I thought he is a rank C?"
    y "Hey, don't ignore me."
    scene bg pic8 with hpunch
    u "Yikes!"
    "Masked men are down."
    y "Should I call the cops now then...?"
    jump Act2B
    
label Act2garden:
    scene bg pic9
    y "(The nearby garden is a good place to get some refreshing air.)"
    y "(There is some people but only elderly and the young, with mothers taking care of their children.)"
    y "(Things look normal until I saw the public toilet is surprisingly busy.)"
    y "(There are five of them and all of them are masked.)"
    y "(All of them have sharp knifes, and one of them even has a gun.)"
    y "(Without thinking twice, I approach them as I just enter the male bathroom awkwardly.)"
    "Why is someone coming in?!"
    "I don't know! Isn't suppose your power to avoid attention be in used?"
    y "I see, you guys are up to not good."
    stop music 
    play music battle
    scene bg pic9 with hpunch
    u "Hah! it's only a small girl, bring her down!"
    y "One of the helmet guy approach me, and he is using brainwashing power."
    "Your power is [power] against 5."
    
    if power <= 20:
        y "Hmm, not bad, but still far from effective."
        "As the man fail to brainwash, [y] move slightly backwards."
        y "But it's not enough to bring me down."
        y "(His power is strong but not as strong as I expected.)"
        y "(A throw to the wall aside, I bring him down regardless.)"
        
    if power <= 40:
        y "Hmm."
        "As the man fail to brainwash, [y] only slightly budged."
        y "A bit surprisingly."
        y "(His power is slow, so I don't see it as much threat.)"
        y "(I knocked him out easily, and now look at the rest grinning.)"
    
    if power > 41:
        y "Hmm."
        "As the man fail to brainwash, [y] isn't affected at all."
        y "Really."
        y "(His power are slow, but I still think I overestimate the impact.)"
        y "(I knocked him out easily, and now look at the rest grinning.)"
    
    u "Who are you!"
    u "Why he can knocked out Ken so easily! I thought he is a rank C?"
    y "Hey, don't ignore me."
    scene bg pic9 with hpunch
    u "Yikes!"
    "Masked men are down."
    y "Should I call the cops now then...?"
    y "It's quite a mess..."
    jump Act2B    

label Act2metro:
    y "(To get to Raven, I have to get into the metro, where I saw some supicious people walking behind alleyway.)"
    y "(I begin to follow them.)"
    y "(There are five of them and all of them are masked.)"
    y "(I can see them taking direction from a device before destroyed them on ground, looks like a power device too.)"
    y "(All of them have gun, and one of them even has a shield.)"
    y "(Without thinking twice, I approach them as I just enter the male bathroom awkwardly.)"
    scene bg pic8
    "Why is someone coming in?!"
    "I don't know! Isn't suppose your power to avoid attention be in used?"
    y "I see, you guys are up to not good."
    stop music 
    play music battle
    scene bg pic8 with hpunch
    u "Hah! it's only a small girl, bring her down!"
    y "One of the helmet guy approach me, and he is using brainwashing power."
    "Your power is [power] against 15."
    
    if power <= 15:
        y "Hmm... quite a power."
        "As the man trying to control over her, [y] move backwards effected by the impact."
        y "But it's not enough to bring me down!"
        y "(His power look strong and are as strong as I expected.)"
        y "(A tought struggle, but I bring him down in the end.)"
    
    if power <= 20:
        y "Hmm, not bad, but still far from effective."
        "As the man fail to brainwash, [y] move slightly backwards."
        y "But it's not enough to bring me down."
        y "(His power is strong but not as strong as I expected.)"
        y "(A throw to the wall aside, I bring him down regardless.)"
        
    if power <= 40:
        y "Hmm."
        "As the man fail to brainwash, [y] only slightly budged."
        y "A bit surprisingly."
        y "(His power is slow, so I don't see it as much threat.)"
        y "(I knocked him out easily, and now look at the rest grinning.)"
    
    if power > 41:
        y "Hmm."
        "As the man fail to brainwash, [y] isn't affected at all."
        y "Really."
        y "(His power are slow, but I still think I overestimate the impact.)"
        y "(I knocked him out easily, and now look at the rest grinning.)"
    
    u "Why he can knocked out Ken so easily! I thought he is a rank C?"
    u "Dude, go!"
    "Your power is [power] against 15."
    
    if power <= 15:
        y "Hmm... quite a punch."
        "As the man punch down, [y] move backwards effected by the impact."
        y "But it's not enough to bring me down!"
        y "(His hands look strong and are as strong as I expected.)"
        y "(A tought fight, but I bring him down regardless.)"
    
    if power <= 20:
        y "Hmm, not bad, quite a punch."
        "As the man punch down, [y] move slightly backwards."
        y "But it's not enough to bring me down."
        y "(His hands look strong but not as strong as I expected.)"
        y "(A throw to the wall aside, I bring him down regardless.)"
        
    if power <= 40:
        y "Hmm."
        "As the man punch down, [y] only slightly budged."
        y "A bit surprisingly."
        y "(His hands are slow, so I don't see it as much threat.)"
        y "(I knocked him out easily, and now look at the rest grinning.)"
    
    if power > 41:
        y "Hmm."
        "As the man punch down, [y] isn't affected at all."
        y "Really."
        y "(His hands are slow, but I think I overestimate the impact.)"
        y "(I knocked him out easily, and now look at the rest grinning.)"
    
    u "Who are you!"
    u "Shoot at her!"
    y "Hey, don't mess things up!"
    scene bg pic8 with hpunch
    u "Yikes!"
    "Masked men are down."
    y "Should I call the cops now then...?"
    jump Act2B
    
label Act2B:
    stop music
    play music voidmusic
    scene bg black-screen
    y "Oh well."
    y "We are back here again, aren't we?"
    v "Yes my goddess."
    v "How's things down there?"
    y "Good? Aren't I suppose to be with Raven now?"
    v "Yes, there will be nothing change unless you want to."
    y "Then why am I here?"
    v "Couple of reasons."
    v "There's a change of things after you engage in the criminal affairs."
    v "Hence, you are here to be told of such thing."
    y "I see."
    y "What's the other reasons?"
    v "I thought it's about Raven?"
    y "..."
    y "Well, I think I like her, so yeah."
    v "Do you want her to be special?"
    v "The world is at your will to change."
    y "..."
    v "You don't seem conviced."
    y "I would like to help her, but also I didn't want to just ignore her past hard work."
    v "..."
    v "It's alright, nothing will change if you say so."
    v "You have the final call."
    
    play music calm
    y "(I wasn't so sure, but I did change some thing.)"
    y "(As well as seeing some possible future... I don't know what I am feeling right now.)"
    y "(Except for this is how god would feel, emptyness and peace for strange reasons.)"
    scene bg pic7b
    r "Hi [y]! I come to see you today!"
    y "Hi Raven, wow it's still early in the morning, did something nice happen?"
    r "Well yeah! I got a position in the Hero Association after help defeating the mysterious super gang in the street!"
    r "One of them tried to mind control me, but his power is too weak, HA!"
    y "That's great! But take care will you, it seems like the new criminal gang is really hard to deal with."
    r "Yeah about that, my face has been broadcasted city wide last night..."
    r "Can I hide here for the time being?"
    y "Yesterday?"
    r "Yes, well... it's a breaking news thing, youngest like us don't watch TV nowdays."
    y "Ah I scare if I miss something haha... well why the sudden hiding?"
    r "A lot of people know me in that area... you know, like..."
    r "I am a three time karate champion or something."
    r "Now I am recruited to be a heroine... soon, I got more attention than usual."
    y "I thought you used to that by now."
    r "Yes but no..."
    y "(Raven blushed face is kinda cute like always.)"
    r "Anyways, I just want to stay out of sight until the heats go down."
    y "No problem Raven, you can stay here as long as you want."
    y "(I wonder how Raven's stats is.)"
    "Raven power stats 5000, power stats 0"
    y "(Wow she's already that strong!)"
    y "(With that strength, she can beat most of the heroes out there.)"
    r "Is there something on my face?"
    y "Hmm, I just think you are cute hehe."
    r "Oh come on [y]."
    y "Hehe."
    
    scene bg pic11
    y "(I thought it's not going to be problematic, maybe.)"
    y "(But for some reason, I have to enlist myself in the Hero Asociation thing for being close to Raven.)"
    r "(Sorry [y] hehe, it seems I have to list my love ones so the bad guys won't target you and my family, hopefully.)"
    y "I guess it will be a fast ride."
    
    scene bg pic12
    s1 "Please wait here, [y]."
    y "Sure..."
    y "(Hopefully it's nothing.)"
    "A few minutes later."
    qq "Hi nice to meet you, [y]"
    y "Hi sir, nice to meet you too."
    a "Hi my name is Ace, the former head for the Hero Association!"
    y "Yes! I have heard of your stories Mr Ace! It's a pleasure to meet you!"
    a "Sorry for my personal request, I need to have few words with you, young girl."
    "Mr Ace signal the room to be clear as the receptionist gets out."
    a "As you know, Raven is our talented girl that we haven't seen for years."
    a "She is as strong as our top hero now, The Justice Man, and she is still young in her power."
    a "As as her friend, you! We like to make sure she didn't end up getting hurt by the bad guys alike, or any bad influences..."
    y "Ok Mr Ace..."
    y "(It seems he's going to talk for a long time.)"
    "Hours later."
    a "Sorry I keep you for this long haha! It's uncommon for people to listen to me this long."
    y "It's ok sir, I know."
    a "Since [y] you are a super yourself, are you interested in growing your own power?"
    y "..."
    a "Ah, I know not eveyone wants to be a hero or such, but you know, it can help you if you want to."
    y "Sure? Thank you Mr Ace."
    a "No problem!"
    y "(Mr Ace is a nice guy, but he is too talkactive and it rubs people in the wrong way.)"
    
    scene bg pic7
    y "(I need to be stronger too, Raven seems to be stronger than me at the moment.)"
    "A stronger floor and a unique space was made for training in the room."
    jump Training2
    
label Training2:
    menu:
     
        "Let's train.":
            jump T3
            
        "Show me my stats.":
            jump Stat2
        
        "I think I am done for today.":
            jump Act3

label Stat2:
    "Your power is [power]"
    jump Training2
            
            
            
label T3:

    if power <= 100:
        "You plan to lift 1 million tons."
        "Almost a big building and 1/6 of a pryamid would weight."
        "You feel like your body is chilling at best."
        "As you move the weight, in a shape of dumbbells made by your goddess creation power."
        "You lift it once, and you already feel your body in full emerged in power."
        $ power += 100
        jump Training2        
    if power <= 500:
        "Next is 10 million tons."
        "It's almost the weight of a city."
        "It's the same shape of dumbbells, and the floor is clearing crying."
        "You lift it carefully, almost trying to feel it's power."
        "You feel unchallenged, and feeling like your body is teasing for more."
        $ power += 500
        jump Training2       
    if power <= 1000:
        "100 trillion tons sound a lot, but you know you can do more."
        "This time it's in the shape of barbells, because you want to view it as 'heavy' maybe."
        "It feels as easy as before, only this time, the ground can't withstand and collapsed a bit, even though you reinforced it more."
        "You quickly fix it before things become out of hand, and the weight is already too little to be notice."
        $ power += 5000
        jump Training2        
    if power >= 5000:
        "There's no heavy enough equipment for you to train."
        "You try to imagine something heavy on your hands, and then lift it once."
        "Although archive little, it's still much more than most can do."
        $ power += 5000
        jump Training2

   
        
#################################        
    #Chapter 3                  #
#################################  
    
label Act3:   
    
    scene bg pic7b
    "Your power is [power]"
    y "It's time to see how am I doing against Raven..."
    "Raven power stats 10000"
    
    if power <= 5000:
        y "(Wow! She grows so fast...)"
        y "I must become stronger than her!"
        y "No, I'm not jealous!"
        y "Go back to last night!"
        jump Training2
    
    if power <= 10000:
        y "(It's close...)"
        y "Do I need to train more?"
        menu:
            "Nah, it's going to be fine.":
                jump Act3Down
            
            "I will play safe.":
                jump Training2
        
    if power > 10000:
        y "Raven might be strong, but I am stronger!"
        jump Act3Down
    
label Act3Down:
    r "Hi [y]."
    "Raven appears at your door."
    r "Come out now! I can hear you in there!"
    y "(Yikes! Does she listen to what I say just now?)"
    y "Hi Raven, you are earlier than last time, haha..."
    r "Yes! And I want to tell you that I get really strong!"
    r "Now I am at least a billion times stronger than Justice Man! You can't believe it!"
    y "Yes!"
    r "Huh?"
    y "Oh I mean, I didn't believe it...?"
    r "Hehe [y], I know if you hide something from me..."
    scene bg pic7b with hpunch
    "Raven push you to bed."
    scene bg pic7b with vpunch
    y "Raven?"
    r "I heard those things you said last night, my training has able me to hear everything this planet has to say."
    r "You are stronger than me, don't you."
    menu:
        "No...?":
            jump Act3r1
        
        "Yes, sorry Raven I lied to you.":
            jump Act3r2
    
label Act3r1:
    stop music
    play music scare
    "For some reason, Raven looks mad."
    r "You lied."
    y "You don't understand! I made you to become..."
    y "I'm sorry Raven."
    r "Why."
    y "(Raven eyes look so sad, I don't even know how to respond.)"
    r "It's ok [y], we can do this next time."
    y "(As weird as it seen, Raven and I both undress ourselves, we have a long and satisfying sex.)"
    y "(Her power constantly overpowered me, as I can feel her hard breasts and nipples shoving into me.)"
    y "(Her tongue is soft yet aggressive, unable to tell wheter or not if she's serious.)"
    y "(Raven ultimately overpowered me completely as she hug and squeeze me with more strength.)"
    y "(I could feel like my body is trying to grow, but nothing can stop Raven's power now...)"
    y "(How can it be?)"
    y "..."
    "The End of Raven Ending."
return
    
label Act3r2:
    stop music
    play music battle2
    y "(Raven and I stare at each other for a minute.)"
    "Raven slightly move away."
    r "I always like you [y]."
    r "But lets see how much you got!"
    scene bg pic13 with hpunch
    "The world changed as both girls are inside a special world out of any realm and existence."
    "Raven power stats 10000000, power stats 10000000"
    if power <= 50000:
        r "Too weak!"
        y "Urgh!"
        r "[y] I thought you want to be strong? Did you forget our promise?"
        y "Not so much, Hah!"
        y "(I try to jump and rush myself to Raven after being struck into one of the planet, it still hurts on my back.)"
        r "Too bad this is the best you can do!"
        "Raven grows her power!"
        "Raven power stats 100000000, power stats 100000000"
        r "I go easy on you, I become ten times stronger!"
        y "Waah! Wait!"
        scene bg pic13 with hpunch
        "A line of planets are destroyed."
        r "Hmm, that's a bit too weak."
        y "Hey! I still here!"
        "Attacks on Raven are uneffective."
        r "Cute! I like you [y]!"
        y "Hehe, I never felt so excited!"
        scene bg pic13 with hpunch
        "Both girls battle for endless time to come."
        "With no winners or losers, and without the constraint of time."
        "It become endless in this realm, forgotten and slept..."
        "Constant War Ending."
        return
    
    if power > 50000:
        r "You are strong, [y]."
        y "(I grab Raven from behind as I push her with my hard and soft breasts.)"
        r "You are also really good at thiiiiS!"
        scene bg pic13 with hpunch
        "Planets are destroyed but Raven is unharm."
        r "Hah, let me see if you worth it!"
        "Raven grows her power!"
        "Raven power stats 1000000000, power stats 1000000000"
        r "I have to become stronger, a hundred times stronger!"
        y "I can do the same and more too!"
        r "Urgh!"
        scene bg pic13 with hpunch
        y "(The realm gets destroyed as I powered up more.)"
        y "(Raven sees she can't catch up, accept her defeat.)"
        r "You are awesome!"
        r "You are making my body so sensitive!"
        y "I don't mind to have some fun with you..."
        y "(As I put my breasts on Raven, I could feel the cracking of her bones.)"
        y "Not even your black hole proof body can do anything against me."
        r "Yes! MORE!"
        y "I love you Raven!"
        "Sophisticate Love Ending."
        return
    
    
        
    

