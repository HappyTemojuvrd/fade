﻿



label Battleroom:
    
    r "Hi, what challenges you want to face today?"
    menu:
        
        "Planet.":
            jump planet
        
        "Star.":
            jump star
        
        "Galaxy.":
            jump galaxy
        
        "River Galaxy.":
            jump river_galaxy
        
        "Universe.":
            jump universe
        
        "Ranu.":
            jump Ranu
        

        "Go back.":
            jump red
            
            
            
            
label planet:
    
    if p <= 30:
        "You can't lift it!"
        $ p += 10
        jump Battleroom
    
    if p <= 100:
        "You barely lift it!"
        $ p += 100
        jump Battleroom
    
    if p >= 101:
        "You can lift it!"
        $ p += 1000
        jump Battleroom
        
label star:
    
    if p <= 1000:
        "You can't lift it!"
        $ p += 1000
        jump Battleroom
    
    if p <= 10000:
        "You barely lift it!"
        $ p += 10000
        jump Battleroom
    
    if p >= 100001:
        "You can lift it!"
        $ p += 100000
        jump Battleroom    

label galaxy:

    if p <= 100000:
        "You can't lift it!"
        $ p += 100000
        jump Battleroom
    
    if p <= 500000:
        "You barely lift it!"
        $ p += 500000
        jump Battleroom
    
    if p >= 500001:
        "You can lift it!"
        $ p += 500001
        jump Battleroom

label river_galaxy:

    if p <= 1000000:
        "You can't lift it!"
        $ p += 1000000
        jump Battleroom
    
    if p <= 5000000:
        "You barely lift it!"
        $ p += 5000000
        jump Battleroom
    
    if p >= 5000001:
        "You can lift it!"
        $ p += 5000001
        jump Battleroom

label universe:

    if p <= 10000000:
        "You can't lift it!"
        $ p += 10000000
        jump Battleroom
    
    if p <= 50000000:
        "You barely lift it!"
        $ p += 50000000
        jump Battleroom
        
    if p >= 50000001:
        "You can lift it!"
        $ p += 50000001
        jump Battleroom    
    
label Ranu:

    if p <= 1000000000:
        r "Haha, you are too weak"
        $ p += 1000000000
        jump Battleroom
    
    if p <= 500000000000:
        r "Not bad!"
        $ p += 5000000000
        jump Battleroom
    
    if p >= 500000000001:
        r "Wow, you are strong!"
        $ p += 5000000001
        jump Battleroom     
    
    
    