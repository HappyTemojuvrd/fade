﻿# Character

define r = Character("Ranu")


label start:

    call CheckEvent from _call_CheckEvent

    "What is your name?"
    $ y = renpy.input("What is your name?")
    if y == "":
        $ y ="Astra"
    r "Welcome [y]!"
    $ p = 10
    "Your power is [p]!"
    r "You will be trying to grow and win against many foe!"
    y "..."
    r "What do you want to do?"
    jump red
        
        
label red:
    
    menu:
        
        "Train.":
            jump Training_Room
        
        "Show my stats":
            jump showstats
        
        "Battle.":
            jump Battleroom
        
        "End.":
            jump end

            
label showstats:
    "Your power is [p]!"
    jump red
            
            
label end:
    
    "You have choose to end the game."
    "This is your score."
    "power: [p]"
return
